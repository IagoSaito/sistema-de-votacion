<%@page import="model.Eleitor"%>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			  $("input").focus(function(){
			    $(this).css("background-color", "#cccccc");
			  });
			  
			  $("input").blur(function(){
			    $(this).css("background-color", "#ffffff");
			  });
			});
		
		function esconderImagem(){
			var e = document.getElementById("imagem");
				e.style.display = 'none';
				e.innerHTML = "Urna eletrônica do nosso país chamado Brasil";
		}
		
		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<style>
			h1 {
			  color: purple;
			  text-align: center;
			  border-color: purple;
			} 
			
			body {
				background-color: pink;
			}
			
			@media screen and (max-width: 900px) {
			img{
			  width: 100%;
			  height: 200px;
			  left: 0;
			  background-color: 
			  }
			}
			
			@media screen and (max-width: 900px) {
			  body {
			    background-image: linear-gradient(to right, rgba(255,0,0,0), rgba(255, 97, 200, 1));;
			  }
			}
			
		</style>
		<title>Sistema Eleitoral do Iago</title>
	</head>
	<body>
		<div class="container">
			<h1 class=page-header>Sistema Eleitoral do Iago</h1>
		</div>
		<form action="ElegerCandidato" method="post">
			<div class="container-fluid">
				<h2 id="tituloPag">Área destinada a eleição de laranjas, milicianos e outros candidatos honestos da nossa nação.</h2>
			</div>
			<div class="container" id="inputLogin">
		           Título <input type="text" required="true" id="titulo" name="titulo">
		           Senha <input type="password" id="senha" name="senha">
		            <button type="submit">Login</button>
		        <div>
		        	<img id="imagem" ondblclick="esconderImagem()" src="./images/eleicoes.jpg" alt="Eleições 2019/" width=1000 height=500>
		        </div>
		        <div class="erroDiv"> 
					${msgErro != null ? msgErro : ''} 
				</div> 
		    </div>
	    </form>
		
		<footer class="container-fluid" id="notaFinal">
	        <h4>Projeto destinado a obtenção de nota na disciplina de Laboratório de Engenharia de Software<br>
	            ministrada pelo professor Fabrício</h4>
	    </footer>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>