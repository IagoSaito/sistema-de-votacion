<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Relatório</title>
	</head>
	<body>
		<h1 class=page-header>Relatório de votos</h1>
		<table border="1" class="container-fluid">
			<thead>
				<tr>
					<td>Candidato</td>
					<td>Voto</td>
				</tr>
			</thead>
			<% 
				Map<String,Integer> relatorio = (HashMap)request.getAttribute("votos"); 
				for (Map.Entry<String,Integer> pair : relatorio.entrySet()) { 
			%>	
			   <tr>
		           <td id=<%= pair.getKey() %>> <%= pair.getKey() %></td>
		           <td> <%= pair.getValue() %></td>
		       </tr>
		    <% } %>	
	    </table>
	    <footer>
			<a href=index.jsp>Home</a>
			<a href="javascript:history.back()">Voltar</a>
		</footer>
		<script>
			document.getElementById("Lula").addEventListener("click", function() {
			  alert("Luiz Inácio Lula da Silva, nascido Luiz Inácio da Silva e mais conhecido como Lula (Caetés, 27 de outubro de 1945[nota 2]), é um político, ex-sindicalista e ex-metalúrgico brasileiro, o 35º presidente do Brasil entre 2003 e 2011. Membro fundador e presidente de honra do Partido dos Trabalhadores, elegeu-se presidente da República na eleição de 2002 e foi reeleito em 2006.");
			});
			
			document.getElementById("Bonoro").addEventListener("click", function() {
				  alert("Jair Messias Bolsonaro (Glicério, 21 de março de 1955) é um militar da reserva, político e atual presidente do Brasil. Filiado ao Partido Social Liberal (PSL), foi deputado federal por sete mandatos entre 1991 e 2018, sendo eleito através de diferentes partidos ao longo de sua carreira. Seu irmão Renato Bolsonaro e três de seus filhos também são políticos: Carlos Bolsonaro (vereador do Rio de Janeiro pelo PSC), Flávio Bolsonaro (senador fluminense pelo PSL e comandante da legenda no estado) e Eduardo Bolsonaro (deputado federal de São Paulo também pelo PSL).");
				});
			
			document.getElementById("Boulos").addEventListener("click", function() {
				  alert("Guilherme Castro Boulos (São Paulo, 19 de junho de 1982) é um professor, filósofo, ativista, político e escritor brasileiro. Filiado ao Partido Socialismo e Liberdade (PSOL), é membro da Coordenação Nacional do Movimento dos Trabalhadores Sem Teto (MTST), sendo reconhecido como uma das principais lideranças da esquerda no Brasil[1][2]. Foi candidato a presidente da República, pelo PSOL, nas eleições gerais no Brasil em 2018.[3]");
				});
		</script>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>