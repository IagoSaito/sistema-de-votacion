<%@page import="model.Eleitor"%>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script type="text/javascript">
	
	function confirmarVoto(){
		var codigoCandidato = document.getElementById("voto").value;
		
		if (codigoCandidato == 13) {
			var e = document.createElement('p');
			e.innerText = "Candidato 13: Lula - PT";
			document.body.appendChild(e);	
			criarBotoes();
			
			var img = document.createElement("IMG");
		    img.src = "./images/lula.jpg";
		    img.style.width = "200px";
		    img.style.height = "200px";
		    document.body.appendChild(img);
		}
		
		if (codigoCandidato == 17) {
			var e = document.createElement('p');
			e.innerText = "Candidato 17: Bonoro - PSL";
			document.body.appendChild(e);	
			criarBotoes();
			
			var img = document.createElement("IMG");
		    img.src = "./images/bolsonaro.jpg";
		    img.style.width = "200px";
		    img.style.height = "200px";
		    document.body.appendChild(img);
		} 
		
		if (codigoCandidato == 50) {
			var e = document.createElement('p');
			e.innerText = "Candidato 50: Boulos - PSOL";
			document.body.appendChild(e);
			criarBotoes();
			
			var img = document.createElement("IMG");
		    img.src = "./images/boulos.png";
		    img.style.width = "200px";
		    img.style.height = "200px";
		    document.body.appendChild(img);
		} 
		
	}
	
	function criarBotoes(){
		document.getElementById('btnVoto').style.display = 'none';
		
		var btnConfirma = document.createElement("button");
		btnConfirma.innerHTML = "Confirma";
		var div = document.getElementsByTagName("div")[2];
		document.body.appendChild(btnConfirma);
		
		var btnCancelar= document.createElement("button");
		btnCancelar.innerHTML = "Cancela";
		var div = document.getElementsByTagName("div")[2];
		document.body.appendChild(btnCancelar);
		
		btnConfirma.addEventListener ("click", function() {
			document.getElementById("myForm").submit();
		});
		
		btnCancelar.addEventListener ("click", function() {
			location.href = "eleger_candidato.jsp";
		});	
	}
</script>
<style type="text/css">
	@font-face {
	  font-family: fonteNutela;
	  src: url(sansation_light.woff);
	}
	
	#p1 {
	  font-family: fonteNutela;
	}
</style>
<title>Sistema Eleitoral</title>
</head>
<body>
	<div class="container">
		<h1 class=page-header id="p1">Escolher Candidato</h1>
	</div>
	<div class="container-fluid">
		<h2>Digite o número do candidato que deseja eleger</h2>
	</div>
	<form id="myForm" action="ConfirmarVoto">
		<div class="container" id="div1">
	    	<input type="text" required="true" id="voto" name="voto">
			<input id="btnVoto" type="button" onclick="confirmarVoto()" value="Votar">
	    </div>
	    <div class="erroDiv"> 
			${msgErro != null ? msgErro : ''} 
		</div>
	</form>
	       
	<footer class="container-fluid">
        <a href=index.jsp>Home</a>
     </footer>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>