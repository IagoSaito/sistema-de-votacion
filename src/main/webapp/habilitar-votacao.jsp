<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Habilitar Votação</title>
	</head>
	<body>
		<div class="container">
			<h1 class=page-header>Digite o título para habilitar a votação</h1>
		</div>
		<form action="HabilitarVotacao" method="get">
			<div class="container-fluid">
				<input type="text" required="true" id="titulo" name="titulo">
				<button id="btnHabilitar" type="submit">Habilitar</button>
			</div>
			<p id="confirmacao"></p>
		</form>
		<%  if ((request.getAttribute("falhaAutenticacao")) != null) {
	            if ((request.getAttribute("falhaAutenticacao").toString().equals("1"))) {
	                out.print("<p class=\"warning\"> Usuário inválido ou já habilitado.</p>");
	            } else {
	            	out.print("<p class=\"warning\"> Usuário autenticado com sucesso.</p>");	
	            }
			}
    	%>  
		<footer>
			<a href=index.jsp>Home</a>
		</footer>
	</body>
</html>