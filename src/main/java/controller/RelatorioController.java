package controller;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="Relatorio", urlPatterns="/Relatorio")
public class RelatorioController extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencia_simples");
		EntityManager em = emf.createEntityManager();
		
		Map<String,Integer> relatorio = new HashMap<String,Integer>();
		
		ServletContext sc = req.getServletContext();
		String queryBusca = "SELECT c.cand_nome as Nome, count('v.Voto_Candidato') as Quantidade " +
				"FROM Voto v INNER JOIN Candidato c ON c.cand_NumeroVoto = v.voto_candidato GROUP BY v.Voto_Candidato ORDER BY v.Voto_Candidato DESC";
		
		Query query = em.createNativeQuery(queryBusca);
		
		@SuppressWarnings("unchecked")
		List<Object[]> votos = query.getResultList();
		
		for (Object[] voto : votos) {
			Integer qtd = ((BigInteger)voto[1]).intValue();
			relatorio.put(voto[0].toString(), qtd);
		}
		req.setAttribute("votos", relatorio);
		sc.getRequestDispatcher("/relatorio.jsp").forward(req, resp);
	}
	
}
