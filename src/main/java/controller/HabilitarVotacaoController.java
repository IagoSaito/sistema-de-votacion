package controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Eleitor;

@WebServlet(name="HabilitarVotacao", urlPatterns = "/HabilitarVotacao")
public class HabilitarVotacaoController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencia_simples");
			EntityManager em = emf.createEntityManager();
			
			Eleitor eleitor = new Eleitor();
			String titulo = req.getParameter("titulo");
			
			eleitor = em.find(Eleitor.class, Integer.parseInt(titulo));
			
			if ((eleitor == null) || (eleitor.getStatusEleitor() >= 2)) {
				req.setAttribute("falhaAutenticacao", 1);
				req.getRequestDispatcher("habilitar-votacao.jsp").forward(req, resp);
			} else if (eleitor.getStatusEleitor() == 1) {
				em.getTransaction().begin();       
		        eleitor.setStatusEleitor(2); 
		        em.getTransaction().commit();
			}
			
			em.close();
			emf.close();
			req.setAttribute("falhaAutenticacao", 2);
			req.getRequestDispatcher("habilitar-votacao.jsp").forward(req, resp);
			
		} catch (Exception e) {
			System.out.println("Falha da habilitação: " + e.getMessage());
		}
	}
}
