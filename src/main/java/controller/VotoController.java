package controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Candidato;
import model.Eleitor;
import model.Voto;

//import Teste.Servlet.Exception;

@WebServlet(name = "ConfirmarVoto", urlPatterns = { "/ConfirmarVoto" })
public class VotoController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencia_simples");
			EntityManager em = emf.createEntityManager();
			
			Candidato candidato;
			Eleitor eleitor;
			Voto v = new Voto();
			String voto = req.getParameter("voto");

			candidato = em.find(Candidato.class, Integer.parseInt(voto));
			
			HttpSession session = req.getSession();
			eleitor = (Eleitor) session.getAttribute("eleitor");
			
			if((candidato == null) || (eleitor.getStatusEleitor() != 2)) {
				req.setAttribute("msgErro", "Voto já efetuado ou candidato inválido");
				req.getRequestDispatcher("eleger_candidato.jsp").forward(req, res);
			}
			else {
				v.setEleitor(eleitor);
				v.setCandidato(candidato);
		        
		        Query query = em.createNativeQuery("INSERT INTO Voto VALUES (null, :eleitor, :candidato)", Voto.class);
				query.setParameter("eleitor", eleitor.getIdEleitor());
				query.setParameter("candidato", candidato.getIdCandidato());
				
				em.getTransaction().begin();
				query.executeUpdate();
				em.getTransaction().commit();
				
				Query queryUpdate = em.createNativeQuery("UPDATE Eleitor SET eleitor_status = 3 WHERE eleitor_titulo = :titulo", Eleitor.class); 
				queryUpdate.setParameter("titulo", eleitor.getIdEleitor());
				em.getTransaction().begin();  
				queryUpdate.executeUpdate();
				em.getTransaction().commit();
			}
			
			em.close();
			emf.close();
			
			res.sendRedirect("voto_concluido.html");
			
		} catch(Exception e) {
			System.out.println("Erro ao dar build com gradle: " + e.getMessage());
		}
	}
}
