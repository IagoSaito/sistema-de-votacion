package controller;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import Teste.Servlet.Exception;

@WebServlet(name = "chefe-secao", urlPatterns = { "/chefe-secao" })
public class ChefeSecaoController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req,
            HttpServletResponse res) throws IOException{
		try {
			res.sendRedirect("chefe_secao.html");
		}catch(Exception e) {
			System.out.println("Erro ao dar build com gradle");
		}
	}
}
