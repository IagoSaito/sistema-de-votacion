package controller;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EleitorDaoImplementacao;
import model.Eleitor;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

@WebServlet(name = "ElegerCandidato", urlPatterns = {"/ElegerCandidato"})
public class LoginController extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		try {

			EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencia_simples");
			EntityManager em = emf.createEntityManager();

			Eleitor eleitor = new Eleitor();
			String titulo = req.getParameter("titulo");

			eleitor = em.find(Eleitor.class, Integer.parseInt(titulo));
			
			em.close();
			emf.close();

			if ((eleitor == null) || (!eleitor.getSenhaEleitor().equals(req.getParameter("senha")))) {
				req.setAttribute("msgErro", "Usuário/Senha inválidos!");
				req.getRequestDispatcher("index.jsp").forward(req, res);
			} 
			
			HttpSession session =  req.getSession();
			session.setAttribute("eleitor", eleitor);
			
			if ((eleitor.getStatusEleitor() == 1) && (eleitor.getCargoEleitor().getIdCargo() == 1)) {
				req.setAttribute("msgErro", "Eleitor não habilitado para voto");
				req.getRequestDispatcher("index.jsp").forward(req, res);
			}
			
			else if ((eleitor.getStatusEleitor() == 3) && (eleitor.getCargoEleitor().getIdCargo() == 1)) {
				req.setAttribute("msgErro", "Eleitor com voto já computado, favor verificar");
				req.getRequestDispatcher("index.jsp").forward(req, res);
			}

			else if (eleitor.getCargoEleitor().getIdCargo() == 2) {
				res.sendRedirect("mesario.html");
			}

			else if (eleitor.getCargoEleitor().getIdCargo() == 3) {
				res.sendRedirect("chefe-secao.html");
			}

			else if (eleitor.getCargoEleitor().getIdCargo() == 1) {
				res.sendRedirect("eleger_candidato.jsp");
			}

		} catch (Exception e) {
			System.out.println("Erro ao dar build com gradle:" + e.getMessage());
		}
	}
}
