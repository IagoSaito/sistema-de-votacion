package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="Eleitor")
public class Eleitor {
	
	@Id
	@Column(name="eleitor_titulo")
	private int idEleitor;
	
	@Column(name="eleitor_nome")
	private String nomeEleitor;
	
	@Column(name="eleitor_senha")
	private String senhaEleitor;
	
	@Column(name="eleitor_zona")
	private String zonaEleitor;
	
	@Column(name="eleitor_municipio")
	private String municipioEleitor;

	@Column(name="eleitor_UF")
	private String UF_Eleitor;
	
	@Column(name="eleitor_nascimento")
	private String nascimentoEleitor;
	
	@Column(name="eleitor_emissao")
	private String emissaoEleitor;
	
	@Column(name="eleitor_status")
	private int statusEleitor;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.ALL})
    @JoinColumn(name="eleitor_cargo")
	private Cargo cargoEleitor;
	
	public Eleitor () {};
	
	public int getIdEleitor() {
		return idEleitor;
	}

	public void setIdEleitor(int idEleitor) {
		this.idEleitor = idEleitor;
	}

	public String getNomeEleitor() {
		return nomeEleitor;
	}

	public void setNomeEleitor(String nomeEleitor) {
		this.nomeEleitor = nomeEleitor;
	}

	public String getSenhaEleitor() {
		return senhaEleitor;
	}

	public void setSenhaEleitor(String senhaEleitor) {
		this.senhaEleitor = senhaEleitor;
	}

	public String getZonaEleitor() {
		return zonaEleitor;
	}

	public void setZonaEleitor(String zonaEleitor) {
		this.zonaEleitor = zonaEleitor;
	}

	public String getMunicipioEleitor() {
		return municipioEleitor;
	}

	public void setMunicipioEleitor(String municipioEleitor) {
		this.municipioEleitor = municipioEleitor;
	}

	public String getUF_Eleitor() {
		return UF_Eleitor;
	}

	public void setUF_Eleitor(String uF_Eleitor) {
		UF_Eleitor = uF_Eleitor;
	}

	public String getNascimentoEleitor() {
		return nascimentoEleitor;
	}

	public void setNascimentoEleitor(String nascimentoEleitor) {
		this.nascimentoEleitor = nascimentoEleitor;
	}

	public String getEmissaoEleitor() {
		return emissaoEleitor;
	}

	public void setEmissaoEleitor(String emissaoEleitor) {
		this.emissaoEleitor = emissaoEleitor;
	}

	public int getStatusEleitor() {
		return statusEleitor;
	}

	public void setStatusEleitor(int statusEleitor) {
		this.statusEleitor = statusEleitor;
	}

	public Cargo getCargoEleitor() {
		return cargoEleitor;
	}

	public void setCargoEleitor(Cargo cargoEleitor) {
		this.cargoEleitor = cargoEleitor;
	}
	
}
