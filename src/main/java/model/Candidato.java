package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Candidato")
public class Candidato {
	
	@Id
	@Column(name="cand_numeroVoto")
	private int idCandidato;
	
	@Column(name="cand_nome")
	private String nomeCandidato;
	
	@Column(name="cand_partido")
	private String partidoCandidato;
	
	public Candidato () {};

	public int getIdCandidato() {
		return idCandidato;
	}

	public void setIdCandidato(int idCandidato) {
		this.idCandidato = idCandidato;
	}

	public String getNomeCandidato() {
		return nomeCandidato;
	}

	public void setNomeCandidato(String nomeCandidato) {
		this.nomeCandidato = nomeCandidato;
	}

	public String getPartidoCandidato() {
		return partidoCandidato;
	}

	public void setPartidoCandidato(String partidoCandidato) {
		this.partidoCandidato = partidoCandidato;
	}
	
	
}
