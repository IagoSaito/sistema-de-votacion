package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Cargo")
public class Cargo {
	
	@Id
	@Column(name="cargo_id")
	private int idCargo;
	
	@Column(name="cargo_descricao")
	private String descricaoCargo;
	
	public Cargo () {};

	public int getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}

	public String getDescricaoCargo() {
		return descricaoCargo;
	}

	public void setDescricaoCargo(String descricaoCargo) {
		this.descricaoCargo = descricaoCargo;
	}
	
}
