package dao;

import java.util.List;

import model.Eleitor;

public interface EleitorDao {
	public Eleitor findByID(String id);
	public Eleitor findByName(String nome);
	public List<Eleitor>listarTodos();
}
